package com.joboffers.offer.domain;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OfferFactory {

    public static Offer cyberSourceOffer() {
        return Offer
                .builder()
                .id("94f14146-9fdf-4d62-ab14-637644fe809b")
                .companyName("Cybersource")
                .position("Software Engineer - Mobile (m/f/d)")
                .salary("4k - 8k PLN")
                .offerUrl("https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn")
                .build();
    }

    public static Offer cdqPolandOffer() {
        return Offer
                .builder()
                .id("94f14146-9fdf-4d62-ab14-637644fe809c")
                .companyName("CDQ Poland")
                .position("Junior DevOps Engineer")
                .salary("8k - 14k PLN")
                .offerUrl("https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd")
                .build();
    }
}
