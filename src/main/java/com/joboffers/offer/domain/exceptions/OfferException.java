package com.joboffers.offer.domain.exceptions;

public class OfferException extends RuntimeException {

    private final OfferError offerError;
    private final String paramInfo;

    public OfferException(OfferError offerError, String paramInfo) {
        this.offerError = offerError;
        this.paramInfo = paramInfo;
    }

    OfferError getOfferError() {
        return offerError;
    }

    @Override
    public String getMessage() {
        return offerError.getMessage(paramInfo);
    }
}
