package com.joboffers.offer.domain.exceptions;

public enum OfferError {

    OFFER_NOT_FOUND("Offer id: %s is not found"),
    OFFER_WITH_URL_EXISTS("Offer with url: %s is exists");

    private final String message;

    OfferError(String message) {
        this.message = message;
    }

    String getMessage(String p) {
        return String.format(message, p);
    }
}
