package com.joboffers.offer.domain.exceptions;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode
@Getter
@Builder
public class OfferErrorResponse {
    private final String message;
}
