package com.joboffers.offer.domain.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class OfferControllerErrorHandler {

    @ExceptionHandler(OfferException.class)
    public ResponseEntity<OfferErrorResponse> handleOfferNotFoundException(OfferException offerException) {
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        if (offerException.getOfferError() == OfferError.OFFER_NOT_FOUND) {
            httpStatus = HttpStatus.NOT_FOUND;
        } else if (offerException.getOfferError() == OfferError.OFFER_WITH_URL_EXISTS) {
            httpStatus = HttpStatus.CONFLICT;
        }
        log.info(offerException.getMessage());
        return ResponseEntity.status(httpStatus).body(OfferErrorResponse.builder().message(offerException.getMessage()).build());
    }
}
