package com.joboffers.offer.domain;

import com.joboffers.infrastracture.offer.dto.JobOfferDto;
import com.joboffers.offer.domain.dto.OfferDto;
import com.joboffers.offer.domain.exceptions.OfferException;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.joboffers.offer.domain.exceptions.OfferError.OFFER_NOT_FOUND;
import static com.joboffers.offer.domain.exceptions.OfferError.OFFER_WITH_URL_EXISTS;

@Service
@RequiredArgsConstructor
public class OfferService {

    private final OfferRepository repository;
    private final OfferMapper offerMapper;

    @Cacheable(cacheNames = "offers")
    public List<OfferDto> findAllOffers() {
        return repository.findAll()
                .stream()
                .map(offerMapper::mapToOfferDto)
                .collect(Collectors.toList());
    }

    public OfferDto findOfferById(String id) {
        return repository
                .findById(id)
                .map(offerMapper::mapToOfferDto)
                .orElseThrow(() -> new OfferException(OFFER_NOT_FOUND, id));
    }

    public List<Offer> saveAll(List<Offer> offers) {
        return repository.saveAll(offers);
    }

    public List<Offer> saveAllJobOffer(List<JobOfferDto> jobOffers) {
        List<Offer> offers = mapOffersWhichNotExist(jobOffers);
        return repository.saveAll(offers);
    }

    public OfferDto saveOffer(OfferDto offerDto) {
        Offer offer = offerMapper.mapToOfferFromOfferDto(offerDto);
        try {
            Offer savedOffer = repository.save(offer);
            return offerMapper.mapToOfferDto(savedOffer);
        } catch (DuplicateKeyException e) {
            throw new OfferException(OFFER_WITH_URL_EXISTS, offer.getOfferUrl());
        }
    }

    private List<Offer> mapOffersWhichNotExist(List<JobOfferDto> jobOffers) {
        return jobOffers.stream()
                .filter(jobOfferDto -> !jobOfferDto.getOfferUrl().isEmpty())
                .filter(jobOfferDto -> !repository.existsByOfferUrl(jobOfferDto.getOfferUrl()))
                .map(offerMapper::mapToOfferFromJobOfferDto)
                .collect(Collectors.toList());
    }
}