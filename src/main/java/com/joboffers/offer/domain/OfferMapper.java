package com.joboffers.offer.domain;

import com.joboffers.infrastracture.offer.dto.JobOfferDto;
import com.joboffers.offer.domain.dto.OfferDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
interface OfferMapper {

    OfferDto mapToOfferDto(Offer offer);

    @Mapping(target = "id", ignore = true)
    Offer mapToOfferFromJobOfferDto(JobOfferDto jobOfferDto);

    @Mapping(target = "id", ignore = true)
    Offer mapToOfferFromOfferDto(OfferDto offerDto);
}
