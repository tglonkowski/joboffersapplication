package com.joboffers.offer.domain;


import lombok.Builder;
import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Value
@Document("offers")
@Builder
public class Offer {

    @Id
    String id;

    @Field("company")
    String companyName;

    @Field("position")
    String position;

    @Field("salary")
    String salary;

    @Field("url")
    @Indexed(unique = true)
    String offerUrl;
}
