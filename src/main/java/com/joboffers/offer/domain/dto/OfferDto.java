package com.joboffers.offer.domain.dto;

import lombok.Builder;
import lombok.Value;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;


@Value
@Builder
public class OfferDto implements Serializable {

    private static long serialVersionUID = 5676987863L;
    @NotBlank
    @Size(min = 4)
    String companyName;
    @NotBlank
    String position;
    @NotBlank
    String salary;
    @NotBlank
    @Indexed(unique = true)
    String offerUrl;
}
