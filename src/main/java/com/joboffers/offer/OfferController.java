package com.joboffers.offer;


import com.joboffers.offer.domain.OfferService;
import com.joboffers.offer.domain.dto.OfferDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static com.joboffers.config.UrlConfig.OFFER_URL;

@RestController
@RequestMapping(OFFER_URL)
@RequiredArgsConstructor
public class OfferController {

    private final OfferService offerService;

    @GetMapping
    public ResponseEntity<List<OfferDto>> findAllOffers() {
        return ResponseEntity.ok(offerService.findAllOffers());
    }

    @GetMapping("/{id}")
    public ResponseEntity<OfferDto> findOfferById(@PathVariable String id) {
        return ResponseEntity.ok(offerService.findOfferById(id));
    }

    @PostMapping
    public ResponseEntity<OfferDto> saveOffer(@Valid @RequestBody OfferDto offerDto) {
        return new ResponseEntity<>(offerDto, HttpStatus.CREATED);
    }
}
