package com.joboffers.offer.scheduling;

import com.joboffers.infrastracture.RemoteOfferClient;
import com.joboffers.infrastracture.offer.dto.JobOfferDto;
import com.joboffers.offer.domain.Offer;
import com.joboffers.offer.domain.OfferService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
class HttpOfferScheduler {

    private final RemoteOfferClient remoteOfferClient;
    private final OfferService offerService;

    private final DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

    @Scheduled(fixedDelayString = "${offer.job.schedule.delay}")
    void get_offers_job() {
        List<JobOfferDto> jobOfferDtos = remoteOfferClient.getOffers();
        List<Offer> offers = offerService.saveAllJobOffer(jobOfferDtos);
        log.info(String.format("Saved new offers: %d, %s", offers.size(), LocalDateTime.now().format(dateTimeFormat)));
    }
}