package com.joboffers.config;

public class UrlConfig {
    public static final String OFFER_URL = "/offers";
    public static final String LOGIN_URL = "/login";
}