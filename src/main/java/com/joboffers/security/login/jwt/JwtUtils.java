package com.joboffers.security.login.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
class JwtUtils {

    @Value("${jwt.auth.expiration.time}")
    private int milisecondsPerDay;
    @Value("${jwt.auth.secret.key}")
    private String mySecret;

    String generateJwtToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("user", userDetails.getUsername());
        return Jwts.builder()
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + milisecondsPerDay))
                .addClaims(claims)
                .signWith(SignatureAlgorithm.HS512, mySecret)
                .compact();
    }

    String extractUsername(String token) {
        return getClaims(token).getSubject();
    }

    boolean validateToken(String token, UserDetails userDetails) {
        String username = extractUsername(token);
        return username.equals(userDetails.getUsername()) && !isJwtTokenExpired(token);
    }

    private boolean isJwtTokenExpired(String token) {
        return getClaims(token).getExpiration().before(new Date());
    }

    private Claims getClaims(String token) {
        return Jwts.parser().setSigningKey(mySecret).parseClaimsJws(token).getBody();
    }
}
