package com.joboffers.security.login.jwt;

import com.joboffers.security.login.domain.dto.LoginRequestDto;


public interface AuthService {
    AuthJwtToken createAuthToken(LoginRequestDto loginRequestDto);
}
