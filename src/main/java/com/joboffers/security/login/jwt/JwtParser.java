package com.joboffers.security.login.jwt;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Component
class JwtParser {

    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER_PREFIX = "Bearer ";

    String parse(HttpServletRequest httpServletRequest) {
        String authHeader = httpServletRequest.getHeader(AUTHORIZATION);
        if (isValidBearerToken(authHeader)) {
            return authHeader.substring(BEARER_PREFIX.length());
        }
        return null;
    }

    private boolean isValidBearerToken(String authHeader) {
        return Objects.nonNull(authHeader) && authHeader.startsWith(BEARER_PREFIX);
    }
}
