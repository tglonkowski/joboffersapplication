package com.joboffers.security.login.jwt;

import com.joboffers.security.login.domain.dto.LoginRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class AuthServiceImpl implements AuthService {

    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetails;
    private final JwtUtils jwtServices;

    @Override
    public AuthJwtToken createAuthToken(LoginRequestDto loginRequestDto) {

        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(loginRequestDto.getUsername(), loginRequestDto.getPassword());
        Authentication authentication = authenticationManager.authenticate(authToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetails userDetails = this.userDetails.loadUserByUsername(loginRequestDto.getUsername());
        String jwtToken = jwtServices.generateJwtToken(userDetails);
        return AuthJwtToken.builder().token(jwtToken).build();
    }
}
