package com.joboffers.security.login.domain.dto;

import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
@Builder
public class LoginRequestDto {
    @NotBlank
    String username;
    @NotBlank
    String password;
}
