package com.joboffers.security.login.domain.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
class LoginControllerErrorHandler {

    @ExceptionHandler(UsernameNotFoundException.class)
    ResponseEntity<LoginErrorResponse> handleUsernameNotFoundException(UsernameNotFoundException exception) {
        log.info(exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(LoginErrorResponse.builder()
                .message(exception.getMessage()).build());
    }
}
