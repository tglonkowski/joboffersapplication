package com.joboffers.security.login.domain.exceptions;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
class LoginErrorResponse {
    String message;
}