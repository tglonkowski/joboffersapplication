package com.joboffers.security.login;

import com.joboffers.security.login.domain.dto.LoginRequestDto;
import com.joboffers.security.login.jwt.AuthJwtToken;
import com.joboffers.security.login.jwt.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static com.joboffers.config.UrlConfig.LOGIN_URL;

@RestController
@RequiredArgsConstructor
@RequestMapping(LOGIN_URL)
class LoginController {

    private final AuthService authService;

    @PostMapping()
    public ResponseEntity<AuthJwtToken> login(@Valid @RequestBody LoginRequestDto loginRequestDto) {
        return ResponseEntity.ok(authService.createAuthToken(loginRequestDto));
    }
}