package com.joboffers.security.login.mongo;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class UserNameNotFoundException {

    private static final String USER_NOT_FOUND = "User is not found: ";

    static UsernameNotFoundException of(String message) {
        return new UsernameNotFoundException(USER_NOT_FOUND + message);
    }
}
