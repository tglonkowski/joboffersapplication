package com.joboffers.security.user;

import com.joboffers.security.user.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.joboffers.security.user.UserMapper.mapToUser;

@Service
@RequiredArgsConstructor
class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public User saveUser(UserDto userDto) {
        User userToSave = mapToUser(userDto);
        return userRepository.save(userToSave);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}
