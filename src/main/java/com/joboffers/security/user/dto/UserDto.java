package com.joboffers.security.user.dto;

import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
@Builder
public class UserDto {
    @NotBlank
    String username;
    @NotBlank
    String password;
}
