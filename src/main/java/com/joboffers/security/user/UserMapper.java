package com.joboffers.security.user;

import com.joboffers.security.user.dto.UserDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCrypt;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class UserMapper {

    static User mapToUser(UserDto userDto) {
        return User.builder()
                .username(userDto.getUsername())
                .password(encodePassword(userDto))
                .build();
    }

    private static String encodePassword(UserDto userDto) {
        return BCrypt.hashpw(userDto.getPassword(), BCrypt.gensalt());
    }
}
