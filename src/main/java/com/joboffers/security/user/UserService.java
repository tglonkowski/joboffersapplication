package com.joboffers.security.user;

import com.joboffers.security.user.dto.UserDto;

import java.util.Optional;

public interface UserService {
    User saveUser(UserDto userDto);

    Optional<User> findByUsername(String username);
}
