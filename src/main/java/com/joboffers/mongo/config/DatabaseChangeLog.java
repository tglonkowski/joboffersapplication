package com.joboffers.mongo.config;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.joboffers.offer.domain.OfferRepository;
import com.joboffers.security.user.UserService;
import com.joboffers.security.user.dto.UserDto;
import org.springframework.context.annotation.Profile;

import static com.joboffers.offer.domain.OfferFactory.cdqPolandOffer;
import static com.joboffers.offer.domain.OfferFactory.cyberSourceOffer;

@ChangeLog(order = "1")
@Profile("!testcontainer")
public class DatabaseChangeLog {

    @ChangeSet(order = "001", id = "seedDatabase", author = "Tomek")
    public void seedDatabase(OfferRepository offerRepository) {
        offerRepository.insert(cyberSourceOffer());
    }

    @ChangeSet(order = "002", id = "seedDatabaseSecondOffer", author = "Tomek")
    public void seedDatabaseSecondOffer(OfferRepository offerRepository) {
        offerRepository.insert(cdqPolandOffer());
    }

    @ChangeSet(order = "003", id = "addUserAdmin", author = "Tomek")
    public void addUserAdmin(UserService userService){
        userService.saveUser(UserDto.builder()
                .username("admin")
                .password("admin")
                .build());
    }
}
