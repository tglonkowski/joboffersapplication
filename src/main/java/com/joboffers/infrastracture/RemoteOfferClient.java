package com.joboffers.infrastracture;

import com.joboffers.infrastracture.offer.dto.JobOfferDto;

import java.util.List;

public interface RemoteOfferClient {

    List<JobOfferDto> getOffers();
}
