package com.joboffers.infrastracture.offer.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class JobOfferDto {
    private String company;
    private String title;
    private String salary;
    private String offerUrl;
}
