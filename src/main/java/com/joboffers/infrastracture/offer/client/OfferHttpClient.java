package com.joboffers.infrastracture.offer.client;

import com.joboffers.infrastracture.RemoteOfferClient;
import com.joboffers.infrastracture.offer.dto.JobOfferDto;
import lombok.AllArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;


@AllArgsConstructor
public class OfferHttpClient implements RemoteOfferClient {

    private final String url;
    private final RestTemplate restTemplate;

    @Override
    public List<JobOfferDto> getOffers() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<HttpHeaders> httpEntity = new HttpEntity<>(httpHeaders);
        try {
            ResponseEntity<List<JobOfferDto>> response = restTemplate.exchange(url, HttpMethod.GET,
                    httpEntity, new ParameterizedTypeReference<List<JobOfferDto>>() {
                    });
            final List<JobOfferDto> body = response.getBody();
            return body != null ? body : Collections.emptyList();
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
