package com.joboffers.offer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.joboffers.offer.domain.OfferControllerConfig;
import com.joboffers.offer.domain.SampleOfferDto;
import com.joboffers.offer.domain.dto.OfferDto;
import com.joboffers.offer.domain.exceptions.OfferErrorResponse;
import com.joboffers.security.login.SampleAuthJwtToken;
import com.joboffers.security.login.SampleLoginRequest;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.context.WebApplicationContext;
import wiremock.org.apache.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

import static com.joboffers.config.UrlConfig.OFFER_URL;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;


@WebMvcTest
@ContextConfiguration(classes = OfferControllerConfig.class)
class OfferControllerTest implements SampleOfferDto, SampleAuthJwtToken, SampleLoginRequest {

    public static final String APPLICATION_JSON = "application/json";

    @Autowired
    WebApplicationContext context;
    @Autowired
    ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        RestAssuredMockMvc.webAppContextSetup(context);
    }

    @Test
    void should_response_status_ok_when_get_request_for_offers() throws JsonProcessingException {
        List<OfferDto> expectedOffersDto = Arrays.asList(cyberSourceOfferDto(), cdqPolandOfferDto());
        String expectedBody = objectMapper.writeValueAsString(expectedOffersDto);

        given()
                .when()
                .get(OFFER_URL)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .contentType(APPLICATION_JSON)
                .body(is(equalTo(expectedBody)));
    }

    @Test
    void should_response_status_ok_when_get_request_for_offer_with_id() throws JsonProcessingException {
        String expectedBody = objectMapper.writeValueAsString(cyberSourceOfferDto());

        given()
                .when()
                .get("/offers/94f14146-9fdf-4d62-ab14-637644fe809b")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .contentType(APPLICATION_JSON)
                .body(is(equalTo(expectedBody)));
    }

    @Test
    void should_response_status_not_found_when_get_request_for_offer_with_unknown_id() throws JsonProcessingException {
        OfferErrorResponse response = OfferErrorResponse.builder().message("Offer id: 3 is not found").build();
        String expectedBody = objectMapper.writeValueAsString(response);

        given()
                .when()
                .get("/offers/3")
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body(is(equalTo(expectedBody)));
    }

    @Test
    void should_response_status_created_when_get_request_to_save_correct_offer() throws JsonProcessingException {
        String expectedBody = objectMapper.writeValueAsString(cyberSourceOfferDto());

        given()
                .accept(APPLICATION_JSON)
                .contentType(APPLICATION_JSON)
                .body(expectedBody)
                .when()
                .post(OFFER_URL)
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .body(is(equalTo(expectedBody)));
    }
}
