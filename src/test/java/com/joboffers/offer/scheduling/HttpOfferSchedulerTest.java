package com.joboffers.offer.scheduling;

import com.joboffers.infrastracture.RemoteOfferClient;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.time.Duration;

import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = "${offer.job.schedule.delay}=PT10s")
public class HttpOfferSchedulerTest {

    @SpyBean
    RemoteOfferClient remoteOfferClient;

    @Test
    void should_get_offers_called_at_least_two_times_during_twenty_seconds(){
        await()
                .atMost(Duration.ofSeconds(20))
                .untilAsserted(() -> verify(remoteOfferClient, atLeast(2)).getOffers());
    }
}
