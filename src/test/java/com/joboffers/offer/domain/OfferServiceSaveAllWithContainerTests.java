package com.joboffers.offer.domain;

import com.joboffers.infrastracture.offer.dto.JobOfferDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Testcontainers
@ActiveProfiles({"container", "testcontainer"})
public class OfferServiceSaveAllWithContainerTests implements SampleOffer {

    @Container
    private static final MongoDBContainer MONGO_DB_CONTAINER = new MongoDBContainer("mongo");

    @BeforeAll
    static void beforeAll() {
        MONGO_DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(MONGO_DB_CONTAINER.getFirstMappedPort()));
    }
 
    @Autowired
    OfferService offerService;

    @Autowired
    OfferRepository offerRepository;

    @Test
    void should_save_two_offers() {
        List<Offer> expectedOffers = Arrays.asList(cyberSourceOffer(), cdqPolandOffer());

        List<Offer> savedOffers = offerService.saveAll(Arrays.asList(cyberSourceOffer(), cdqPolandOffer()));

        assertThat(offerRepository.findAll()).containsAnyElementsOf(savedOffers);
        assertThat(savedOffers).isEqualTo(expectedOffers);
    }

    @Test
    void should_save_only_one_unique_job_offer() {
        Offer offer = new Offer("1",
                "testCompanyOffer", "testPositionOffer"
                , "testSalaryOffer", "notUniqueURL");

        offerService.saveAll(Collections.singletonList(offer));

        JobOfferDto firstJobOffer = new JobOfferDto(
                "testCompany1",
                "testTitle1",
                "testSalary1",
                "notUniqueURL");

        JobOfferDto secondJobOffer = new JobOfferDto(
                "testCompany2",
                "testTitle2",
                "testSalary2",
                "UniqueURL");

        List<Offer> offers = offerService.saveAllJobOffer(Arrays.asList(firstJobOffer, secondJobOffer));

        assertThat(offers.size()).isEqualTo(1);
        assertThat(offerService.findAllOffers().size()).isEqualTo(2);
    }
}