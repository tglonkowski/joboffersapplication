package com.joboffers.offer.domain.exceptions;

import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

class OfferControllerErrorHandlerTest {

    @Test
    void should_return_error_response_when_handle_offer_not_found_exception() {
        OfferControllerErrorHandler errorHandler = new OfferControllerErrorHandler();
        final OfferException exception = new OfferException(OfferError.OFFER_NOT_FOUND, String.valueOf(3));
        final OfferErrorResponse expectedResponse = new OfferErrorResponse(exception.getMessage());

        ResponseEntity<OfferErrorResponse> responseEntity = errorHandler.handleOfferNotFoundException(exception);

        assertThat(responseEntity.getBody()).isEqualTo(expectedResponse);
    }

    @Test
    void should_return_error_response_when_handle_offer_with_url_exists_exception() {
        OfferControllerErrorHandler errorHandler = new OfferControllerErrorHandler();
        final OfferException exception = new OfferException(OfferError.OFFER_WITH_URL_EXISTS, "test");
        final OfferErrorResponse expectedResponse = new OfferErrorResponse(exception.getMessage());

        ResponseEntity<OfferErrorResponse> responseEntity = errorHandler.handleOfferNotFoundException(exception);

        assertThat(responseEntity.getBody()).isEqualTo(expectedResponse);
    }
}
