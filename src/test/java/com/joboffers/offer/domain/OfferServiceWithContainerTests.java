package com.joboffers.offer.domain;

import com.joboffers.JobOffersApplication;
import com.joboffers.offer.domain.dto.OfferDto;
import com.joboffers.offer.domain.exceptions.OfferError;
import com.joboffers.offer.domain.exceptions.OfferException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


@SpringBootTest(classes = JobOffersApplication.class)
@Testcontainers
@ActiveProfiles("container")
class OfferServiceWithContainerTests implements SampleOfferDto {

    @Container
    private static final MongoDBContainer MONGO_DB_CONTAINER = new MongoDBContainer("mongo");

    @BeforeAll
    static void beforeAll() {
        MONGO_DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(MONGO_DB_CONTAINER.getFirstMappedPort()));
    }

    @Autowired
    OfferService offerService;

    @Test
    void should_return_all_offers() {
        List<OfferDto> expectedOffers = Arrays.asList(cyberSourceOfferDto(), cdqPolandOfferDto(), testOfferDto());

        List<OfferDto> offersDto = offerService.findAllOffers();

        assertThat(offersDto).isEqualTo(expectedOffers);
    }

    @Test
    void should_return_offer_when_id_given() {
        OfferDto expectedOfferDto = cdqPolandOfferDto();

        OfferDto offerDto = offerService.findOfferById("94f14146-9fdf-4d62-ab14-637644fe809c");

        assertThat(offerDto).isEqualTo(expectedOfferDto);
    }

    @Test
    void should_throw_offer_not_found_exception_with_unknown_id() {
        assertThatThrownBy(() ->
                offerService.findOfferById("1"))
                .isInstanceOf(OfferException.class)
                .hasMessage("Offer id: 1 is not found")
                .hasFieldOrPropertyWithValue("offerError", OfferError.OFFER_NOT_FOUND);
    }

    @Test
    void should_return_same_offer_dto_when_given_offer_dto_to_save() {
        OfferDto expectedOfferDto = testOfferDto();

        OfferDto offerDto = offerService.saveOffer(expectedOfferDto);

        assertThat(offerDto).isEqualTo(expectedOfferDto);
    }

    @Test
    void should_throw_offer_exist_exception_when_saving_offer_second_time() {
        assertThatThrownBy(() ->
                offerService.saveOffer(cyberSourceOfferDto()))
                .isInstanceOf(OfferException.class)
                .hasMessage("Offer with url: https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn is exists")
                .hasFieldOrPropertyWithValue("offerError", OfferError.OFFER_WITH_URL_EXISTS);
    }
}
