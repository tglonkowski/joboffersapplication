package com.joboffers.offer.domain;

import com.joboffers.offer.domain.dto.OfferDto;
import com.joboffers.offer.domain.exceptions.OfferException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.dao.DuplicateKeyException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


class OfferServiceTest implements SampleOffer, SampleOfferDto {

    OfferRepository offerRepository = Mockito.mock(OfferRepository.class);
    OfferMapper offerMapper = Mockito.mock(OfferMapper.class);
    OfferService offerService;
    
    @BeforeEach
    void setUp() {
        offerService = new OfferService(offerRepository, offerMapper);
    }

    @Test
    void should_return_all_offers() {
        when(offerRepository.findAll()).thenReturn(Arrays.asList(cyberSourceOffer(), cdqPolandOffer()));
        List<OfferDto> expectedOffersDto = Arrays.asList(cyberSourceOfferDto(), cdqPolandOfferDto());

        List<OfferDto> offersDto = offerService.findAllOffers();

        assertThat(offersDto.size()).isEqualTo(expectedOffersDto.size());
    }

    @Test
    void should_return_offer_for_given_id() {
        when(offerRepository.findById("94f14146-9fdf-4d62-ab14-637644fe809b")).thenReturn(Optional.of(cyberSourceOffer()));
        OfferDto offerDto = cyberSourceOfferDto();
        when(offerMapper.mapToOfferDto(any(Offer.class))).thenReturn(offerDto);

        OfferDto offerDtoById = offerService.findOfferById("94f14146-9fdf-4d62-ab14-637644fe809b");

        assertThat(offerDtoById).isEqualTo(offerDto);
    }

    @Test
    void should_return_offer_for_given_two_id() {
        when(offerRepository.findById("94f14146-9fdf-4d62-ab14-637644fe809c")).thenReturn(Optional.of(cdqPolandOffer()));
        OfferDto offerDto = cdqPolandOfferDto();
        when(offerMapper.mapToOfferDto(any(Offer.class))).thenReturn(offerDto);

        OfferDto offerDtoById = offerService.findOfferById("94f14146-9fdf-4d62-ab14-637644fe809c");

        assertThat(offerDtoById).isEqualTo(offerDto);
    }

    @Test()
    void should_throw_offer_not_found_exception_with_unknown_id() {
        assertThatThrownBy(() ->
                offerService.findOfferById("5"))
                .isInstanceOf(OfferException.class)
                .hasMessage("Offer id: 5 is not found");
    }

    @Test
    void should_return_offer_dto_after_save_offer() {
        Offer offerSaved = cyberSourceOffer();
        when(offerRepository.save(any(Offer.class))).thenReturn(offerSaved);
        OfferDto offerDto = cyberSourceOfferDto();
        when(offerMapper.mapToOfferDto(any(Offer.class))).thenReturn(offerDto);
        when(offerMapper.mapToOfferFromOfferDto(any(OfferDto.class))).thenReturn(offerSaved);


        OfferDto offerDtoSaved = offerService.saveOffer(offerDto);

        assertThat(offerDtoSaved).isEqualTo(offerDto);
    }

    @Test
    void should_throw_offer_url_exists_exception_with_existing_offer_url() {
        when(offerRepository.save(any(Offer.class))).thenThrow(DuplicateKeyException.class);
        OfferDto offerDto = cyberSourceOfferDto();
        Offer offerSaved = cyberSourceOffer();
        when(offerMapper.mapToOfferFromOfferDto(any(OfferDto.class))).thenReturn(offerSaved);

        assertThatThrownBy(() ->
                offerService.saveOffer(offerDto))
                .isInstanceOf(OfferException.class)
                .hasMessage("Offer with url: " + offerDto.getOfferUrl() + " is exists");
    }
}
