package com.joboffers.offer.domain;

import com.joboffers.offer.OfferController;
import com.joboffers.offer.domain.dto.OfferDto;
import com.joboffers.offer.domain.exceptions.OfferControllerErrorHandler;
import com.joboffers.offer.domain.exceptions.OfferError;
import com.joboffers.offer.domain.exceptions.OfferException;
import com.joboffers.security.SecurityConfig;
import com.joboffers.security.login.jwt.JwtTestConfig;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import java.util.Arrays;
import java.util.List;

@Import({JwtTestConfig.class, SecurityConfig.class})
public class OfferControllerConfig implements SampleOfferDto {

    @Bean
    OfferService offerService() {
        OfferRepository offerRepository = Mockito.mock(OfferRepository.class);
        OfferMapper offerMapper = Mockito.mock(OfferMapper.class);
        return new OfferService(offerRepository, offerMapper) {

            @Override
            public List<OfferDto> findAllOffers() {
                return Arrays.asList(cyberSourceOfferDto(), cdqPolandOfferDto());
            }

            @Override
            public OfferDto findOfferById(String id) {
                if (id.equals("94f14146-9fdf-4d62-ab14-637644fe809b")) {
                    return cyberSourceOfferDto();
                } else if (id.equals("94f14146-9fdf-4d62-ab14-637644fe809c")) {
                    return cdqPolandOfferDto();
                }
                throw new OfferException(OfferError.OFFER_NOT_FOUND, id);
            }
        };
    }

    @Bean
    OfferController offerController(OfferService aOfferService) {
        return new OfferController(aOfferService);
    }

    @Bean
    OfferControllerErrorHandler errorHandler() {
        return new OfferControllerErrorHandler();
    }
}