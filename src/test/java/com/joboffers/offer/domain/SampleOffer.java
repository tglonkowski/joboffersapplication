package com.joboffers.offer.domain;

interface SampleOffer {

    default Offer cyberSourceOffer() {
        return new Offer("94f14146-9fdf-4d62-ab14-637644fe809b",
                "Cybersource",
                "Software Engineer - Mobile (m/f/d)",
                "4k - 8k PLN",
                "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn");
    }

    default Offer cdqPolandOffer() {
        return new Offer("94f14146-9fdf-4d62-ab14-637644fe809c",
                "CDQ Poland",
                "Junior DevOps Engineer",
                "8k - 14k PLN",
                "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd");
    }
}
