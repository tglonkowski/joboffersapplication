package com.joboffers.offer.domain;

import com.joboffers.offer.domain.dto.OfferDto;

public interface SampleOfferDto {

    default OfferDto cyberSourceOfferDto() {
        return OfferDto.builder()
                .companyName("Cybersource")
                .position("Software Engineer - Mobile (m/f/d)")
                .salary("4k - 8k PLN")
                .offerUrl("https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn")
                .build();
    }

    default OfferDto cdqPolandOfferDto() {
        return OfferDto.builder()
                .companyName("CDQ Poland")
                .position("Junior DevOps Engineer")
                .salary("8k - 14k PLN")
                .offerUrl("https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd")
                .build();
    }

    default OfferDto testOfferDto() {
        return OfferDto.builder()
                .companyName("TestCompanyName")
                .position("TestPosition")
                .salary("TestSalary")
                .offerUrl("TestOfferUrl")
                .build();
    }
}
