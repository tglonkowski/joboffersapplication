package com.joboffers.config;

import com.joboffers.infrastracture.RemoteOfferClient;
import org.springframework.web.client.RestTemplate;


public class OfferHttpClientTestConfig extends Config {

    public RemoteOfferClient remoteOfferTestClient(String url, int connectionTimeout, int readTimeout) {
        final RestTemplate restTemplate = restTemplate(connectionTimeout, readTimeout, restTemplateResponseErrorHandler());
        return offerHttpClient(url, restTemplate);
    }
}
