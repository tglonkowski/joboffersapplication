package com.joboffers.redis;

import com.joboffers.JobOffersApplication;
import com.joboffers.offer.domain.OfferRepository;
import com.joboffers.offer.domain.OfferService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.time.Duration;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = JobOffersApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Testcontainers
@ActiveProfiles("redistest")
class RedisCacheContainerTest {

    @Container
    public static final GenericContainer REDIS =  new GenericContainer(DockerImageName.parse("redis")).withExposedPorts(6379);
    public static final String CACHE_NAME = "offers";

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private OfferService offerService;

    @MockBean
    private OfferRepository offerRepository;

    @BeforeAll
    static void beforeAll(){
        REDIS.start();
        System.setProperty("spring.redis.port", String.valueOf(REDIS.getFirstMappedPort()));
    }

    @Test
    void should_cache_contains_offers_after_invocation_offer_service_twice_to_find_all_offers() {
        assertThat(cacheManager.getCacheNames().isEmpty());

        offerService.findAllOffers();
        offerService.findAllOffers();

        assertThat(cacheManager.getCacheNames().contains(CACHE_NAME));
    }

    @Test
    void should_invoke_two_times_offer_repo_after_10_seconds() {

        final Duration duration = Duration.ofSeconds(10);
        final int expectedTimesOfInvocation = 2;
        offerService.findAllOffers();
        assertThat(cacheManager.getCacheNames().contains(CACHE_NAME));

        await()
                .atMost(duration)
                .untilAsserted(() -> {
                            offerService.findAllOffers();
                            verify(offerRepository, atLeast(expectedTimesOfInvocation)).findAll();
                        }
                );
    }
}