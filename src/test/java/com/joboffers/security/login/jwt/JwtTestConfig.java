package com.joboffers.security.login.jwt;


import com.joboffers.security.login.SampleAuthJwtToken;
import com.joboffers.security.login.mongo.MongoUserDetailsService;
import com.joboffers.security.user.UserService;
import org.mockito.Mock;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestConfiguration
public class JwtTestConfig implements SampleAuthJwtToken {

    @Mock
    UserService userService;

    @Bean
    JwtUtils jwtUtils() {
        JwtUtils jwtUtils = mock(JwtUtils.class);
        when(jwtUtils.extractUsername(any())).thenReturn("admin");
        when(jwtUtils.validateToken(any(), any())).thenReturn(true);
        return jwtUtils;
    }

    @Bean
    JwtParser jwtParser() {
        JwtParser jwtParser = mock(JwtParser.class);
        when(jwtParser.parse(any())).thenReturn(jwtToken().getToken());
        return jwtParser;
    }

    @Bean
    UserDetailsService userDetailsService() {
        return new MongoUserDetailsService(userService) {
            @Override
            public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
                return new User("admin", "admin", Collections.emptyList());
            }
        };
    }

    @Bean
    AuthTokenFilter authTokenFilter() {
        return new AuthTokenFilter(jwtUtils(), jwtParser(), userDetailsService());
    }

    @Bean
    JwtAuthEntryPoint authenticationEntryPoint() {
        return new JwtAuthEntryPoint();
    }
}