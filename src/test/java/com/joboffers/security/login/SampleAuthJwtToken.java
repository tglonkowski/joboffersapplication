package com.joboffers.security.login;

import com.joboffers.security.login.jwt.AuthJwtToken;

public interface SampleAuthJwtToken {
    default AuthJwtToken jwtToken() {
        return AuthJwtToken.builder()
                .token("exampleJwtToken")
                .build();
    }
}