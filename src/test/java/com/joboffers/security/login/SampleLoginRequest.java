package com.joboffers.security.login;

import com.joboffers.security.login.domain.dto.LoginRequestDto;

public interface SampleLoginRequest {
    default LoginRequestDto loginRequestDto() {
        return LoginRequestDto.builder()
                .username("username")
                .password("password")
                .build();
    }

    default String loginJson() {
        return "  { \"username\": \"admin\",\n" +
                "    \"password\": \"admin\"\n" +
                "  }";
    }


    default String wrongLoginJson() {
        return "  { \"username\": \"wrongUsername\",\n" +
                "    \"password\": \"password\"\n" +
                "  }";
    }

    default String wrongPasswordJson() {
        return "  { \"username\": \"username\",\n" +
                "    \"password\": \"wrongPassword\"\n" +
                "  }";
    }
}