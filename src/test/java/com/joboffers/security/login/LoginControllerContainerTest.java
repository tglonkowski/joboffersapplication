package com.joboffers.security.login;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.joboffers.security.login.jwt.AuthJwtToken;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static com.joboffers.config.UrlConfig.LOGIN_URL;
import static com.joboffers.config.UrlConfig.OFFER_URL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Testcontainers
@ActiveProfiles("container")
@AutoConfigureMockMvc
public class LoginControllerContainerTest implements SampleLoginRequest {

    @Container
    private static final MongoDBContainer MONGO_DB_CONTAINER = new MongoDBContainer("mongo");

    @BeforeAll
    static void beforeAll() {
        MONGO_DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(MONGO_DB_CONTAINER.getFirstMappedPort()));
    }

    @Autowired
    LoginController loginController;
    @Autowired
    MockMvc mockMvc;
    @Autowired
    ObjectMapper objectMapper;

    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER_PREFIX = "Bearer ";

    @Test
    void should_return_unauthorized_http_code_when_given_wrong_login() throws Exception {
        int expectedValue = HttpStatus.UNAUTHORIZED.value();

        MvcResult mvcResult = mockMvc.perform(post(LOGIN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(wrongLoginJson()))
                .andExpect(status().isUnauthorized())
                .andReturn();

        int actualValue = mvcResult.getResponse().getStatus();

        assertThat(actualValue).isEqualTo(expectedValue);
    }

    @Test
    void should_return_unauthorized_http_code_when_given_wrong_password() throws Exception {
        int expectedValue = HttpStatus.UNAUTHORIZED.value();

        MvcResult mvcResult = mockMvc.perform(post(LOGIN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(wrongPasswordJson()))
                .andExpect(status().isUnauthorized())
                .andReturn();

        int actualValue = mvcResult.getResponse().getStatus();

        assertThat(actualValue).isEqualTo(expectedValue);
    }


    @Test
    void should_return_jwt_token_after_user_authenticate() throws Exception {

        MvcResult mvcResult = mockMvc.perform(post(LOGIN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(loginJson()))
                .andExpect(status().isOk())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        AuthJwtToken authJwtToken = objectMapper.readValue(content, AuthJwtToken.class);
        String token = authJwtToken.getToken();
        assertThat(token).isNotNull();
    }

    @Test
    void should_return_ok_http_code_when_user_is_authenticate_by_jwt_token() throws Exception {

        MvcResult mvcResult = mockMvc.perform(post(LOGIN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(loginJson()))
                .andExpect(status().isOk())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        AuthJwtToken authJwtToken = objectMapper.readValue(content, AuthJwtToken.class);
        String token = authJwtToken.getToken();

        mockMvc.perform(get(OFFER_URL)
                .header(AUTHORIZATION, BEARER_PREFIX + token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void should_return_unauthorized_http_code_when_user_is_not_authenticate_by_jwt_token() throws Exception {

        mockMvc.perform(post(LOGIN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(loginJson()))
                .andExpect(status().isOk())
                .andReturn();

        mockMvc.perform(get(OFFER_URL))
                .andExpect(status().isUnauthorized())
                .andReturn();
    }
}