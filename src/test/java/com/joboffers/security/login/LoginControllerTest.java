package com.joboffers.security.login;

import com.joboffers.security.login.domain.dto.LoginRequestDto;
import com.joboffers.security.login.jwt.AuthJwtToken;
import com.joboffers.security.login.jwt.AuthService;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class LoginControllerTest implements SampleLoginRequest, SampleAuthJwtToken {

    @Test
    void should_return_correct_jwt_token() {
        LoginRequestDto loginRequestDto = loginRequestDto();
        AuthService authService = mock(AuthService.class);
        when(authService.createAuthToken(loginRequestDto)).thenReturn(jwtToken());
        LoginController loginController = new LoginController(authService);

        AuthJwtToken expectedJwtToken = loginController.login(loginRequestDto).getBody();

        assertThat(expectedJwtToken).isEqualTo(jwtToken());
    }
}