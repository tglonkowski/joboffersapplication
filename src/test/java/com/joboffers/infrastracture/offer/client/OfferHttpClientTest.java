package com.joboffers.infrastracture.offer.client;

import com.joboffers.infrastracture.offer.dto.JobOfferDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

class OfferHttpClientTest implements SampleRestTemplateExchangeResponse, SampleOfferResponse, SampleOffer {

    public static final int SIZE_OF_ONE = 1;
    public static final int SIZE_OF_TWO = 2;
    public static final String OFFER_URL = "http://programming-masterpiece.com:5057/offers";

    @Test
    void should_return_one_element_from_offer_list() {
        //given
        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        ResponseEntity<List<JobOfferDto>> response = responseWithOneOffer();
        when(getExchange(restTemplate)).thenReturn(response);
        OfferHttpClient offerHttpClient = new OfferHttpClient(OFFER_URL, restTemplate);

        //when
        List<JobOfferDto> offers = offerHttpClient.getOffers();

        //then
        assertThat(offers.size()).isEqualTo(SIZE_OF_ONE);
    }

    @Test
    void should_return_two_element_from_offer_list() {
        //given
        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        ResponseEntity<List<JobOfferDto>> response = responseWithTwoOffers(emptyOffer(), emptyOffer());
        when(getExchange(restTemplate)).thenReturn(response);

        OfferHttpClient offerHttpClient = new OfferHttpClient(OFFER_URL, restTemplate);

        //when
        List<JobOfferDto> offers = offerHttpClient.getOffers();

        //then
        assertThat(offers.size()).isEqualTo(SIZE_OF_TWO);
    }

    @Test
    void should_return_empty_offer_list() {
        //given
        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        ResponseEntity<List<JobOfferDto>> response = responseWithZeroOffer();
        when(getExchange(restTemplate)).thenReturn(response);
        OfferHttpClient offerHttpClient = new OfferHttpClient(OFFER_URL, restTemplate);

        //when
        List<JobOfferDto> offers = offerHttpClient.getOffers();

        //then
        assertThat(offers.size()).isZero();
    }
}