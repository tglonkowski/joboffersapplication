package com.joboffers.infrastracture.offer.client;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.joboffers.config.OfferHttpClientTestConfig;
import com.joboffers.infrastracture.RemoteOfferClient;
import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.SocketUtils;
import wiremock.org.apache.http.HttpStatus;

import java.util.Arrays;
import java.util.Collections;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static com.joboffers.config.UrlConfig.OFFER_URL;
import static org.assertj.core.api.BDDAssertions.then;

class OfferHttpClientIntegrationTest implements SampleOffer {

    public static final String CONTENT_TYPE = "Content-type";
    public static final String APPLICATION_JSON = "application/json";

    int port = SocketUtils.findAvailableTcpPort();
    WireMockServer wireMockServer;
    RemoteOfferClient remoteOfferClient = new OfferHttpClientTestConfig().remoteOfferTestClient("http://localhost:" + port + OFFER_URL, 1000, 1000);

    @BeforeEach
    void setUp() {
        wireMockServer = new WireMockServer(options().port(port));
        wireMockServer.start();
        WireMock.configureFor(port);
    }

    @AfterEach
    void tearDown() {
        wireMockServer.stop();
    }

    @Test
    void should_return_two_job_offers() {
        WireMock.stubFor(WireMock.get(OFFER_URL)
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON)
                        .withBody(twoOffersJson())));

        then(remoteOfferClient.getOffers())
                .containsExactlyInAnyOrderElementsOf(Arrays.asList(cyberSourceOffer(), cdqPolandOffer()));
    }

    @Test
    void should_return_one_job_offer() {
        WireMock.stubFor(WireMock.get(OFFER_URL)
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON)
                        .withBody(oneOfferJson())));

        then(remoteOfferClient.getOffers())
                .containsExactlyInAnyOrderElementsOf(Collections.singletonList(cyberSourceOffer()));
    }

    @Test
    void should_return_zero_offers_when_status_is_ok() {
        WireMock.stubFor(WireMock.get(OFFER_URL)
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON)
                        .withBody(zeroOfferJson())));

        then(remoteOfferClient.getOffers().size()).isZero();
    }

    @Test
    void should_return_zero_offers_when_status_is_no_content() {
        WireMock.stubFor(WireMock.get(OFFER_URL)
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_NO_CONTENT)
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON)
                        .withBody(zeroOfferJson())));

        then(remoteOfferClient.getOffers().size()).isZero();
    }

    @Test
    void should_fail_with_empty_response() {
        WireMock.stubFor(WireMock.get(OFFER_URL)
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON)
                        .withFault(Fault.EMPTY_RESPONSE)));

        then(remoteOfferClient.getOffers().size()).isZero();
    }

    @Test
    void should_fail_with_random_data_response() {
        WireMock.stubFor(WireMock.get(OFFER_URL)
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON)
                        .withFault(Fault.RANDOM_DATA_THEN_CLOSE)));

        then(remoteOfferClient.getOffers().size()).isZero();
    }

    @Test
    void should_fail_when_connection_fails() {
        WireMock.stubFor(WireMock.get(OFFER_URL)
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON)
                        .withFault(Fault.CONNECTION_RESET_BY_PEER)));

        then(remoteOfferClient.getOffers().size()).isZero();
    }

    @Test
    void should_fail_with_malformed() {
        WireMock.stubFor(WireMock.get(OFFER_URL)
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON)
                        .withFault(Fault.MALFORMED_RESPONSE_CHUNK)));

        then(remoteOfferClient.getOffers().size()).isZero();
    }

    @Test
    void should_response_zero_offers_when_response_delay_is_over_1000() {
        WireMock.stubFor(WireMock.get(OFFER_URL)
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON)
                        .withFixedDelay(Integer.MAX_VALUE)));

        then(remoteOfferClient.getOffers().size()).isZero();
    }

    @Test
    void should_response_one_offer_when_response_delay_is_500() {
        WireMock.stubFor(WireMock.get(OFFER_URL)
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON)
                        .withFixedDelay(500)
                        .withBody(oneOfferJson())));

        then(remoteOfferClient.getOffers().size()).isOne();
    }

    @Test
    void should_return_message_error_while_using_http_client_when_http_client_return_internal_server_exception() {
        WireMock.stubFor(WireMock.get(OFFER_URL)
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON)));

        BDDAssertions.assertThatThrownBy(() -> remoteOfferClient.getOffers()).hasMessageContaining("Error while using Http client");
    }

    @Test
    void should_return_response_not_found_status_exception_when_http_client_return_not_found_exception() {
        WireMock.stubFor(WireMock.get(OFFER_URL)
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_NOT_FOUND)
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON)));

        BDDAssertions.assertThatThrownBy(() -> remoteOfferClient.getOffers()).hasMessage("404 NOT_FOUND");
    }

    @Test
    void should_return_response_unauthorized_exception_when_http_client_return_unauthorized_exception() {
        WireMock.stubFor(WireMock.get(OFFER_URL)
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_UNAUTHORIZED)
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON)));

        BDDAssertions.assertThatThrownBy(() -> remoteOfferClient.getOffers()).hasMessage("401 UNAUTHORIZED");
    }
}
