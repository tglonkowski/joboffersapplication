package com.joboffers.infrastracture.offer.client;

import com.joboffers.infrastracture.offer.dto.JobOfferDto;

public interface SampleOffer {

    default JobOfferDto emptyOffer() {
        return new JobOfferDto();
    }

    default JobOfferDto exampleOfferDto(String company, String position, String salary, String url) {
        return JobOfferDto.builder()
                .company(company)
                .title(position)
                .salary(salary)
                .offerUrl(url)
                .build();
    }

    default String twoOffersJson() {
        return "[{\n" +
                "    \"title\": \"Software Engineer - Mobile (m/f/d)\",\n" +
                "    \"company\": \"Cybersource\",\n" +
                "    \"salary\": \"4k - 8k PLN\",\n" +
                "    \"offerUrl\": \"https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"title\": \"Junior DevOps Engineer\",\n" +
                "    \"company\": \"CDQ Poland\",\n" +
                "    \"salary\": \"8k - 14k PLN\",\n" +
                "    \"offerUrl\": \"https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd\"\n" +
                "  }]";
    }

    default String oneOfferJson() {
        return "[{\n" +
                "    \"title\": \"Software Engineer - Mobile (m/f/d)\",\n" +
                "    \"company\": \"Cybersource\",\n" +
                "    \"salary\": \"4k - 8k PLN\",\n" +
                "    \"offerUrl\": \"https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn\"\n" +
                "  }]";
    }

    default String zeroOfferJson() {
        return "[]";
    }

    default JobOfferDto cyberSourceOffer() {
        return exampleOfferDto("Cybersource", "Software Engineer - Mobile (m/f/d)", "4k - 8k PLN", "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn");
    }

    default JobOfferDto cdqPolandOffer() {
        return exampleOfferDto("CDQ Poland", "Junior DevOps Engineer", "8k - 14k PLN", "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd");
    }

}
