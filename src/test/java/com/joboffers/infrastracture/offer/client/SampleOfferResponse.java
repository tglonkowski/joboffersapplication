package com.joboffers.infrastracture.offer.client;

import com.joboffers.infrastracture.offer.dto.JobOfferDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public interface SampleOfferResponse {

    default ResponseEntity<List<JobOfferDto>> responseWithOneOffer() {
        return new ResponseEntity<>(Collections.singletonList(new JobOfferDto()), HttpStatus.ACCEPTED);
    }

    default ResponseEntity<List<JobOfferDto>> responseWithZeroOffer() {
        return new ResponseEntity<>(Collections.emptyList(), HttpStatus.ACCEPTED);
    }

    default ResponseEntity<List<JobOfferDto>> responseWithTwoOffers(JobOfferDto firstOfferDto, JobOfferDto secondOfferDto) {
        return new ResponseEntity<>(Arrays.asList(firstOfferDto, secondOfferDto), HttpStatus.ACCEPTED);
    }
}
